class ResponsesController < ApplicationController

  
  def create
    @response = Response.new(response_params)
    @response.user_id = current_user.id
    respond_to do |format|
      if @response.save
        format.html { redirect_to petition_path(@response.petition), notice: 'Вы Успешно Прошли Опрос' }
      else
        format.html { redirect_to petition_path(@response.petition), notice: 'Что-то Пошло не так' }
      end
    end
  end


  private

    def response_params
      params.require(:response).permit(:user_id, :petition_id, response_questions_attributes: [:id, :question_id, :text, answer_ids: []])
    end
end
