class PetitionsController < ApplicationController
  before_filter :authenticate_user!,only: [:new, :edit, :index]
  before_action :set_petition, only: [:show, :edit, :update, :destroy, :vote, :st, :add, :categories]
  layout 'admin', only: [:new,:edit, :index]
  # GET /petitions
  # GET /petitions.json
  def index
    if current_user.role_id ==1 or current_user.role_id == 3
      @petitions = Petition.all.page(params[:page]).per(10)
    else
      @petitions = Petition.where(user:current_user).page(params[:page]).per(10)
    end


  end

  def vote
    @petition.vote.users << current_user if @petition.user != current_user or current_user.role_id == [1,3]
    if @petition.save
      redirect_to @petition
    end
  end
  def add
    @petition.like.users << current_user if @petition.user != current_user or current_user.role_id == [1,3]
    respond_to do |format|
      format.html {redirect_to :back, notice: "Liked!"}
      format.js
    end

  end
  def st

    @petition.status = Status.find(2)
    if @petition.save
      redirect_to petitions_path
    end

  end
  # GET /petitions/1
  # GET /petitions/1.json
  def show

    if @petition.category.name == "Опросы"
      @title = "Нравится"
      @untitle = "Понравилось"
      @all = "Всего понравилось:"
      else
      @title = "Проголосовать"
      @untitle = "Проголосовано"
      @all = "Всего голосов:"
    end


  end
  def categories

    if @petition.category.name == "Опросы"
      @title = "Нравится"
      @untitle = "Понравилось"
      @all = "Всего понравилось:"
      else
      @title = "Проголосовать"
      @untitle = "Проголосовано"
      @all = "Всего голосов:"
    end


  end

  # GET /petitions/new
  def new
    @petition = Petition.new
  end

  # GET /petitions/1/edit
  def edit
  end

  # POST /petitions
  # POST /petitions.json
  def create
    @petition = Petition.new(petition_params)
    @petition.user_id = current_user.id
    respond_to do |format|
      if @petition.save
        format.html { redirect_to @petition, notice: 'Ваша публикация создана' }
        format.json { render :show, status: :created, location: @petition }
      else
        format.html { render :new }
        format.json { render json: @petition.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /petitions/1
  # PATCH/PUT /petitions/1.json
  def update
    respond_to do |format|
      if @petition.update(petition_params)
        format.html { redirect_to petitions_path, notice: 'Данные Обновлены' }
        format.json { render :show, status: :ok, location: @petition }
      else
        format.html { render :edit }
        format.json { render json: @petition.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /petitions/1
  # DELETE /petitions/1.json
  def destroy
    @petition.destroy
    respond_to do |format|
      format.html { redirect_to petitions_url, notice: 'Публикация Удалена' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_petition
      @petition = Petition.find_by_slug(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def petition_params
      params.require(:petition).permit(:from,:name, :text, :status_id, :vote_id, :like_id, :category_id, :user_id, :foto, questions_attributes: [:id, :text,:active, :status ,:_destoy, answers_attributes:[:id, :text, :_destroy]])
    end
  end
