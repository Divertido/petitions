class HomeController < ApplicationController
  before_filter :authenticate_user!, only: [:lk,:vote,:admin_category, :admins, :moderators]
  layout 'admin', only:[:lk,:vote,:page, :admins, :moderators]
  def index
  end

  def search

        @petitions = Petition.where(status_id: 2).search(params[:search]).page(params[:page]).per(10)

        home
        add_breadcrumb "Поиск"
  end

  def contact
  end

  def about
  end
  def partner
    @partner = Page.find_by(name: "Партнёры")
  end
  def page
    @categories = Category.all.page(params[:page]).per(10)
  end

  def lk
  end
  def vote
    @petitions = Petition.where(vote: current_user.vote).page(params[:page]).per(10)

  end
  
  before_action :super_admin?, only: [:admins, :moderators]
  # GET /users
  # GET /users.json
  def super_admin?
    unless current_user.id == 2
      redirect_to petitions_path
    end
  end



  def admins

    @users = User.where(role_id: 1).where.not(id: 2).page(params[:page]).per(10)
    home
    add_breadcrumb "Администраторы", :admins_path
  end

  def moderators
   @users = User.where(role_id: 3).page(params[:page]).per(10)
   home
   add_breadcrumb "Модераторы", :moderators_path
 end
end
