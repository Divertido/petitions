class AdsController < ApplicationController
  before_action :set_ad, only: [:show, :edit, :update, :destroy]
  layout "admin"
 


 

  # GET /ads/1/edit
  def edit
  end


  def update
    respond_to do |format|
      if @ad.update(ad_params)
        format.html { redirect_to root_path, notice: 'Данные обновлены' }
        format.json { render :show, status: :ok, location: @ad }
      else
        format.html { render :edit }
        format.json { render json: @ad.errors, status: :unprocessable_entity }
      end
    end
  end



  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ad
      @ad = Ad.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ad_params
      params.require(:ad).permit(:text)
    end
end
