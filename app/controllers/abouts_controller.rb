class AboutsController < ApplicationController
  before_action :set_about, only: [:show, :edit, :update, :destroy]
layout "admin"
 
  def edit
  end

  
  def update
    respond_to do |format|
      if @about.update(about_params)
        format.html { redirect_to '/about', notice: 'Данные обновлены' }
        format.json { render :show, status: :ok, location: @about }
      else
        format.html { render :edit }
        format.json { render json: @about.errors, status: :unprocessable_entity }
      end
    end
  end

 

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_about
      @about = About.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def about_params
      params.require(:about).permit(:text)
    end
end
