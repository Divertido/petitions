class UsersController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_user, only: [:show, :edit, :update, :destroy]
layout "admin"
before_action :super_admin?, only: [:index]
  # GET /users
  # GET /users.json
  def super_admin?
    unless current_user.id == 2
      redirect_to petitions_path
    end
  end

  def index
    @users = User.where(role_id: 2).page(params[:page]).per(10)
    home
    add_breadcrumb "Пользователи", :users_path
  end

  # GET /users/1
  # GET /users/1.json
  def show
    home
      add_breadcrumb "Пользователи", :users_path if current_user.role_id == 1
    add_breadcrumb "#{@user.name}"
  end

  # GET /users/new
  def new
    @user = User.new
    home
    add_breadcrumb "Пользователи", :users_path
    cr
  end

  # GET /users/1/edit
  def edit
    home
      add_breadcrumb "Пользователи", :users_path if current_user.role_id == 1
    ed
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to users_path, notice: 'Пользователь Создан' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
  if user_params[:password].blank?
    user_params.delete(:password)
    user_params.delete(:password_confirmation)
  end

  successfully_updated = if needs_password?(@user, user_params)
                           @user.update(user_params)
                         else
                           @user.update_without_password(user_params)
                         end

  respond_to do |format|
    if successfully_updated
      format.html { redirect_to petitions_path, notice: 'Данные обновлены' }
      format.json { head :no_content }
    else
      format.html { render action: 'edit' }
      format.json { render json: @user.errors, status: :unprocessable_entity }
    end
  end
end



  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'Пользователь Удалён' }
      format.json { head :no_content }
    end
  end



  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :surname, :father_name, :tel, :role_id, :email, :password, :password_confirmation, :foto, question_ids: [], answer_ids: [], survey_ids: [], option_ids: [], options_attributes: [:id, :text, :answer_id ], responses_attributes: [:id, :survey_id, :question_id, :answer_id])
    end
    protected
    def needs_password?(user, params)
      params[:password].present?
    end
end
