class ApplicationController < ActionController::Base


  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :configure_permitted_parameters, if: :devise_controller?



  rescue_from CanCan::AccessDenied do |exception|
    flash[:error] = "Вам не разрешено это действие!"
    end


  def home
      add_breadcrumb "Главная", :root_path
    end
    def cr
      add_breadcrumb "Создание"
    end
    def ed
    add_breadcrumb "Редактировать"
  end
  def lk_breadcrumb
    if current_user.role_id == 1
    add_breadcrumb "Личный Кабинет", admin_path
  else
    add_breadcrumb "Личный Кабинет", private_path
  end
  end



  protected

  def configure_permitted_parameters
  devise_parameter_sanitizer.permit(:sign_up) do  |u|
     u.permit( :foto, :name, :email, :password, :password_confirmation, :tel ,:surname, :father_name)
end
  devise_parameter_sanitizer.permit(:account_update) do |u|
    u.permit(:name, :email, :password, :tel ,:surname, :father_name, :foto)
  end
  end
   def after_update_path_for(resource)
   petitions_path
  end
  def after_sign_in_path_for(resource)
    petitions_path
  end
end
