class FormsController < ApplicationController
  

  def create
    @form = Form.new(form_params)

    respond_to do |format|
      if @form.save
         UserMailer.send_form(@form).deliver_later
        format.html { redirect_to '/contact', notice: 'Ваше сообщение отправлено' }
        format.json { render :show, status: :created, location: @form }
      else
        format.html { render :new }
        format.json { render json: @form.errors, status: :unprocessable_entity }
      end
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions

    # Never trust parameters from the scary internet, only allow the white list through.
    def form_params
      params.require(:form).permit(:text, :name, :email)
    end
end
