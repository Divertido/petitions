class Page < ActiveRecord::Base
	before_destroy :des
	before_create :assign_slug
	before_update :assign_slug
	validates :name , uniqueness: { case_sensitive: false }
	has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "missing.png"
	validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

	def to_param
		"#{slug}"
	end

	def assign_slug
		self.slug = self.name.to_slug_param(locale: :ru)
	end

	def des
		self.image = nil
		self.save
	end



end
