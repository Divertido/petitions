class Question < ActiveRecord::Base
	belongs_to :petition
	has_many :response_questions, dependent: :destroy
	has_many :responses, through: :response_questions
	has_many :answers, dependent: :destroy
	accepts_nested_attributes_for :answers, reject_if: lambda { |a| a[:text].blank? }, allow_destroy: true

end
