class ResponseAnswer < ActiveRecord::Base
  belongs_to :answer
  belongs_to :response_question
end
