class ResponseQuestion < ActiveRecord::Base
  belongs_to :question
  belongs_to :response
  has_many :response_answers, dependent: :destroy
  has_many :answers, through: :response_answers

end
