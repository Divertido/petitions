class Petition < ActiveRecord::Base
  #assiciations
  belongs_to :status
  belongs_to :vote
  belongs_to :like
  belongs_to :category
  belongs_to :user

  has_many :responses, dependent: :destroy
  has_many :users, through: :responses
  has_many :questions, dependent: :destroy
  accepts_nested_attributes_for :questions, reject_if: lambda { |a| a[:text].blank? }, allow_destroy: true

  #Callbacks
  before_create :vot
  before_create :add
  before_save :st
  before_create :assign_slug
  before_update :assign_slug
  before_destroy :des

  #Methods
  def des
    self.foto = nil
    self.save
  end
  def vot
    self.vote = Vote.create
  end
  def add
    self.like = Like.create
  end
  def st
    self.status = Status.find(1) if self.status.nil?
  end

  # def slug
  #   name.mb_chars.downcase.gsub(" ", "-").to_s  
  # end
  def assign_slug
    self.slug = self.name.to_slug_param(locale: :ru)
  end

  def to_param 
   "#{slug}"
 end

 has_attached_file :foto, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "missing.png"
 validates_attachment_content_type :foto, content_type: /\Aimage\/.*\Z/


 def self.search(search)
  if search
    self.where("name LIKE ? OR name LIKE ? OR name LIKE ? OR name LIKE ?", "%#{search.mb_chars.capitalize.to_s}%", "%#{search.mb_chars.to_s}%", "%#{search.mb_chars.upcase.to_s}%", "%#{search.mb_chars.downcase.to_s}%" )
  else
    self.all
  end
end
end
