class Ability
  include CanCan::Ability

  def initialize(user)
    alias_action :create, :read, :update, :destroy, :to => :crud
      user ||= User.new # guest user (not logged in)
      if user.admin?
        can :manage, :all
      elsif user.moder?
        can :manage, Petition
        can :read, Category
        cannot :vote , Petition
        can :update, User do |u|
   u.try(:user) == user
                          end  
   elsif user.user?
     can :read , Petition
     can :read , Category
     can :update, User do |u|
u.try(:user) == user
                        end
     can :update, Petition do |u|
u.try(:user) == user
                            end
    elsif user
    can :read , all
   
   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
