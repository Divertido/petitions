class Category < ActiveRecord::Base
  has_many :petitions, dependent: :destroy
  before_destroy :des
  before_create :assign_slug
  before_update :assign_slug

  has_attached_file :foto, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "missing.png"
  validates_attachment_content_type :foto, content_type: /\Aimage\/.*\Z/

  def to_param
    "#{slug}"
  end

  def assign_slug
    self.slug = self.name.to_slug_param(locale: :ru)
  end

  def des
    self.foto = nil
    self.save
  end
end
