class Response < ActiveRecord::Base
	belongs_to :user
	belongs_to :petition
	has_many :questions, through: :response_questions
	has_many :response_questions, dependent: :destroy
	accepts_nested_attributes_for :response_questions, allow_destroy: true

end
