class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
  :recoverable, :rememberable, :trackable, :validatable
  has_many :petitions, dependent: :destroy
  belongs_to :role
  belongs_to :like
  has_and_belongs_to_many :vote
  has_many :responses, dependent: :destroy
  # has_many :petitions, through: :responses
  # has_many :petitions


  before_destroy :des

  has_attached_file :foto, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "missing.png"
  validates_attachment_content_type :foto, content_type: /\Aimage\/.*\Z/

  scope :admin?, -> {where(role_id: 1)}

  def des
    self.foto = nil
    self.save
  end

 #  def to_param
 #   "#{name}"
 # end
 before_save :assign_role
 def assign_role
  self.role = Role.find_by name: "Пользователь" if self.role.nil?
end
def admin?
  self.role.name == "Админ"
end
def moder?
  self.role.name == "Модератор"
end
def user?
  self.role.name == "Пользователь"
end
end
