class Answer < ActiveRecord::Base
	belongs_to :question
	has_many :response_answers, dependent: :destroy
	has_many :response_questions, through: :response_answers
end
