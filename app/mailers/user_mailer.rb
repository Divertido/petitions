class UserMailer < ApplicationMailer
	default from: "info@vlastnaroda.org"
	def send_form(form)
		@form = form
		mail(to: "info@vlastnaroda.org", subject: 'Со страницы Контакты')
	end
end
