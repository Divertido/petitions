class ApplicationMailer < ActionMailer::Base
  default from: "info@vlastnaroda.org"
  layout 'mailer'
end
