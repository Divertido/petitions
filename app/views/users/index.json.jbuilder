json.array!(@users) do |user|
  json.extract! user, :id, :name, :surname, :father_name, :tel, :role_id
  json.url user_url(user, format: :json)
end
