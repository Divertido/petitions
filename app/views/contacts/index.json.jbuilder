json.array!(@contacts) do |contact|
  json.extract! contact, :id, :text
  json.url contact_url(contact, format: :json)
end
