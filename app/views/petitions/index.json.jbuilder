json.array!(@petitions) do |petition|
  json.extract! petition, :id, :name, :text, :status, :vote_id, :like_id, :category_id, :user_id
  json.url petition_url(petition, format: :json)
end
