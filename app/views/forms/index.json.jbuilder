json.array!(@forms) do |form|
  json.extract! form, :id, :text, :name, :email
  json.url form_url(form, format: :json)
end
