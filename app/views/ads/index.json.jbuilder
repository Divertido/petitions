json.array!(@ads) do |ad|
  json.extract! ad, :id, :text
  json.url ad_url(ad, format: :json)
end
