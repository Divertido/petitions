Rails.application.routes.draw do
 

  resources :responses
  devise_for :users
  resources :users 
  
  

  # resources :surveys 
  
  # resources :questions
  # resources :answers
  # resources :responses

  resources :ads
  resources :abouts
  resources :contacts
  resources :forms
  resources :pages

  get '/search' =>  'home#search'

  get '/contact' => 'home#contact'

  get '/about' =>  'home#about'
  get '/partners' =>  'home#partner'

  get '/lk' =>  'home#lk'
  get '/vote' =>  'home#vote'
  get '/admin_category' =>  'home#page'

  get '/moderators' =>  'home#moderators'
  get '/admins' =>  'home#admins'

  
  resources :categories
  
  resources :petitions do
    member do
      put 'vote'
      put 'st'
      put 'add'
    end
  end
  get '/categories/:category_id/:id' => 'petitions#categories', as: :category_petitions
  root 'home#index'
end
