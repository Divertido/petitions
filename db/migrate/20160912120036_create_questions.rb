class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.text :text
      t.boolean :status
      t.boolean :active
      t.belongs_to :petition, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
