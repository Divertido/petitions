class CreateForms < ActiveRecord::Migration
  def change
    create_table :forms do |t|
      t.text :text
      t.string :name
      t.string :email

      t.timestamps null: false
    end
  end
end
