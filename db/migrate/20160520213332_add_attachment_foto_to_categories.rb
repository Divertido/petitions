class AddAttachmentFotoToCategories < ActiveRecord::Migration
  def self.up
    change_table :categories do |t|
      t.attachment :foto
    end
  end

  def self.down
    remove_attachment :categories, :foto
  end
end
