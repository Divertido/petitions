class CreatePetitions < ActiveRecord::Migration
  def change
    create_table :petitions do |t|
      t.string :name
      t.string :from
      t.text :text
      t.belongs_to :status, index: true, foreign_key: true
      t.belongs_to :vote, index: true, foreign_key: true
      t.belongs_to :like, index: true, foreign_key: true
      t.belongs_to :category, index: true, foreign_key: true
      t.belongs_to :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
