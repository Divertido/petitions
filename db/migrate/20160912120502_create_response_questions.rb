class CreateResponseQuestions < ActiveRecord::Migration
  def change
    create_table :response_questions do |t|
      t.belongs_to :question, index: true, foreign_key: true
      t.belongs_to :response, index: true, foreign_key: true
      t.text :text

      t.timestamps null: false
    end
  end
end
