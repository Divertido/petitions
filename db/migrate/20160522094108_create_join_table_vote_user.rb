class CreateJoinTableVoteUser < ActiveRecord::Migration
  def change
    create_join_table :users, :votes do |t|
      # t.index [:user_id, :vote_id]
      # t.index [:vote_id, :user_id]
    end
  end
end
