class AddAttachmentFotoToPetitions < ActiveRecord::Migration
  def self.up
    change_table :petitions do |t|
      t.attachment :foto
    end
  end

  def self.down
    remove_attachment :petitions, :foto
  end
end
