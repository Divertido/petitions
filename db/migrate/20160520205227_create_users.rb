class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :surname
      t.string :father_name
      t.string :tel
      t.belongs_to :role, index: true, foreign_key: true
      t.belongs_to :like, index: true, foreign_key: true
      

      t.timestamps null: false
    end
  end
end
