# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

r1 = Role.create(name:'Админ')
r2 = Role.create(name:'Пользователь')
r3 = Role.create(name:'Модератор')

u1 = User.create(name: 'Ivan', surname: 'Ivanov', father_name: 'Ivanovich', email: 'user@user.user', password: '987654321', password_confirmation: '987654321', tel: '89516543212', role: r2)
u2 = User.create(name: 'Admin', surname: 'Admin', father_name: 'Admin', email: 'admin@admin.admin', password: '987654321', password_confirmation: '987654321', tel: '99999999999', role: r1)
u3 = User.create(name: 'Moder', surname: 'Moder', father_name: 'Moder', email: 'm@m.m', password: '987654321', password_confirmation: '987654321', tel: '96543214565', role: r3)

c1 = Category.create(name: 'First')
c2 = Category.create(name: 'Second')
c3 = Category.create(name: 'Last')

s1 = Status.create(name: 'Не опубликовано')
s2 = Status.create(name: 'Опубликовано')

p1 = Petition.create(slug: "петиция1",name: 'Петиция1', text: '123qweasdzxc', status:s1, vote: Vote.create, category: c1, like: Like.create, user: u1 )
p2 = Petition.create(slug: "петиция2",name: 'Петиция2', text: 'zxcasdqwe123', status:s2, vote: Vote.create, category: c2, like: Like.create, user: u1 )
p3 = Petition.create(slug: "петиция3",name: 'Петиция3', text: 'asdqwezxc321', status:s2, vote: Vote.create, category: c2, like: Like.create, user: u2 )
p4 = Petition.create(slug: "петиция4",name: 'Петиция4', text: 'qweasdzxc123', status:s1, vote: Vote.create, category: c3, like: Like.create, user: u2 )
p5 = Petition.create(slug: "петиция5",name: 'Петиция5', text: 'qweasdzxc123', status:s2, vote: Vote.create, category: c3, like: Like.create, user: u2 )
p6 = Petition.create(slug: "петиция6",name: 'Петиция6', text: 'qweasdzxc123', status:s2, vote: Vote.create, category: c3, like: Like.create, user: u2 )

contact = Contact.create(text:"Введите текст для Контакты")
about = About.create(text:"Введите текст для О проекте")
ad = Ad.create(text:"Введите текст для Рекламы")
partner = Page.create(name: "Партнёры", text:"Введите текст для страницы Партнёры", title: "Наши Партнёры")


q1 = Question.create(text: "Вопрос №1", petition: p5, status: false, active: false)
q2 = Question.create(text: "Вопрос №2", petition: p5, status: false, active: true)
q3 = Question.create(text: "Вопрос №3", petition: p6, status: false, active: false)
q4 = Question.create(text: "Вопрос №4", petition: p6, status: true, active: true)

a1 = Answer.create(text: "Ответ№1", question: q1)
a2 = Answer.create(text: "Ответ№2", question: q1)
a3 = Answer.create(text: "Ответ№3", question: q1)

a4 = Answer.create(text: "Ответ№4", question: q2)
a5 = Answer.create(text: "Ответ№5", question: q2)
a6 = Answer.create(text: "Ответ№6", question: q2)

a7 = Answer.create(text: "Ответ№7", question: q3)
a8 = Answer.create(text: "Ответ№8", question: q3)
a9 = Answer.create(text: "Ответ№9", question: q3)

a10 = Answer.create(text: "Ответ№10", question: q4)
a11 = Answer.create(text: "Ответ№11", question: q4)
a12 = Answer.create(text: "Ответ№12", question: q4)
